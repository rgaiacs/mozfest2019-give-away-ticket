# Mozilla Festival 2019 Give Away Ticket

Because I received a ticket as facilitator for [Mozilla Festival 2019](https://www.mozillafestival.org/),
I'm giving away the ticket that I bought before.

## Form

Register using [Google Form](https://forms.gle/rkcjKKtK7fr4XMZW7) to participate in the draw.

## Winner Announcement

On Friday, 11 October, 2019.

## Data Collected

To take part in the draw,
you only need to provide your email address.
If you are the winner,
I will use the email address that you provided to contact you
with details to assign the ticket to you.

**All** email addresses will be removed deleted after Mozilla Festival 2019:
28 October, 2019.